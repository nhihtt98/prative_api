import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import reportWebVitals from './reportWebVitals';
import Blog from "./components/Blog/Blog"
import BlogDetail from "./components/Blog/BlogDetail"
import {
  BrowserRouter as Router,
  Switch,
  Route,

} from "react-router-dom"

ReactDOM.render(
  <React.StrictMode>
    <Router>
      <App>
        <Switch>
          <Route path ="/blog" component = {Blog} />
          <Route path ="/blog-detail/:id" component = {BlogDetail}/>
        </Switch>
      </App>
    </Router>
    
  </React.StrictMode>,
  document.getElementById('root')
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
