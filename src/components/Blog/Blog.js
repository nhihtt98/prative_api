import React,{ Component } from "react";
import axios from "axios";
import { Link } from "react-router-dom"

class Blog extends Component{
    constructor(props) {
        super(props);
        this.state = {
            items: []
           
        }
    }
    componentDidMount(){
        axios.get("http://localhost/laravel/public/api/blog" )
        .then( res => {
            console.log(res.data.blog.data)
            
            this.setState({
                items: res.data.blog.data
                
            })
        }).catch(error => console.log(error))
    }

    fetchData(){
        let {items} = this.state
        console.log(items)
        if(items.length>0){
            return items.map((value, index) => {
                return(
                    <div>
                         <div className="single-blog-post">
                            <h3>{value['title']}</h3>
                            <div className="post-meta">
                            <ul>
                                <li><i className="fa fa-user" /> Mac Doe</li>
                                <li><i className="fa fa-clock-o" /> 1:33 pm</li>
                                <li><i className="fa fa-calendar" /> DEC 5, 2013</li>
                            </ul>
                            <span>
                                <i className="fa fa-star" />
                                <i className="fa fa-star" />
                                <i className="fa fa-star" />
                                <i className="fa fa-star" />
                                <i className="fa fa-star-half-o" />
                            </span>
                            </div>
                            <a href>
                            <img src={"http://localhost/laravel/public/upload/Blog/image/" + value['image']}  />
                            </a>
                            <p>{value['description']}</p>
                            <Link to={"/blog-detail/"+ value['id']} className="btn btn-primary">Read More</Link>
                        </div>
                    </div>
                   
                )
            })
        }
    }
    render() {
        return (
           
                <div className="col-sm-9">
                    <div className="blog-post-area">
                    <h2 className="title text-center">Latest From our Blog</h2>
                    <>
                    {this.fetchData()}
                    </>
                    
                    <div className="pagination-area">
                        <ul className="pagination">
                        <li><a href className="active">1</a></li>
                        <li><a href>2</a></li>
                        <li><a href>3</a></li>
                        <li><a href><i className="fa fa-angle-double-right" /></a></li>
                        </ul>
                    </div>
                    </div>
                </div>
            
        );
    }
    
}
export default Blog




// - dung de quan ly soucrecode
// - 1 du an: 3-4
// - leader xem va review, test. 


// - tao du an tren git :leader tap
// - lay git ve may minh : moi member lay code ve lam: git clone url
// - copi nhung file minh lam dua vao git 
// - add nhung vao git : git add -A
// - ghi chu phan do lam chi: git commit -a -m "ghi vao day"
// - dua code len git: git push origin ten-nhanh-dang-lam

// conflic: code bi merge lon xon voi nhau => loi 

// - dung lenh 
// - giao dien


// add 
// edit 
// xoa