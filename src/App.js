import logo from './logo.svg';
import './App.css';
import { Component } from 'react';
import Head from "./components/Layout/Head"
import Footer from "./components/Layout/Footer"
import MenuLeft from "./components/Layout/MenuLeft"
import Slider from "./components/Layout/Slider"


class App extends Component{
  constructor(props){
    super(props)
  }
  render() {
    return (
      <>
        <Head/>
        <Slider/>
        <section>
          <div className="container">
          <div className="row">
            <MenuLeft/>
            {this.props.children}
          </div>
          </div>
                
        </section>
        <Footer/>
      </>
    );
  }
}

export default App;
